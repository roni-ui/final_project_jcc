import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Schema from '@ioc:Adonis/Lucid/Schema'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        try {
            const payload = await request.validate(UserValidator)

            const newUser = await User.create({
                name: payload.name,
                password: payload.password,
                email: payload.email
            })

            let otp_code: number = Math.floor(100000 + Math.random() * 900000)
            await Database.table('otp_codes').insert({ otp_code: otp_code, user_id: newUser.id })

            await Mail.send((message) => {
                message
                    .from('admin@sanberdev.com')
                    .to(payload.email)
                    .subject('Welcome Onboard!')
                    .htmlView('mail/otp_verification', { name: payload.name, otp_code: otp_code })
            })

            return response.created({status: 'registered!', data: newUser, message: 'Silakan lakukan verifikasi kode otp yang telah dikirim ke email Anda!'})
        } catch (error) {
            return response.unprocessableEntity({message: error.message})
            
        }
    }

    public async login({ request, response, auth }: HttpContextContract) {
        try {
            const UsersSchema = schema.create({
                email: schema.string(),
                password: schema.string()
            })

            await request.validate({ schema:UsersSchema })
            const email = request.input('email')
            const password = request.input('password')

            const token = await auth.use('api').attempt(email, password)

            return response.ok({message: 'login success', token})
        } catch (error) {
            if (error.guard) {
                return response.badRequest({ message: 'login error', error: error.message })
            } else {
                return response.badRequest({ message: 'login error', error: error.message })
            }
        }
    }

    public async otp_verification({ request, response }: HttpContextContract) {
        const otp_code = request.input('otp_code')
        const email = request.input('email')

        const user = await User.findByOrFail('email', email)

        const dataOtp = await Database.from('otp_codes').where('otp_code', otp_code).firstOrFail()
        if (user.id == dataOtp.user_id) {
            user.isVerified = true
            await user.save()

            return response.ok({ status: 'success', data: 'verification succeeded!' })
        } else {
            return response.badRequest({  status: 'error', data: 'otp verification failed'})
        }
    }
}
