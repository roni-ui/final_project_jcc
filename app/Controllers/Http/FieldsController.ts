import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import FieldValidator from 'App/Validators/FieldValidator';
import Venue from 'App/Models/Venue'

export default class FieldsController {

    public async store({ request, response, params }: HttpContextContract){
        const venue = await Venue.findByOrFail('id', params.venue_id)

        const newField = new Field()
        newField.name = request.input('name')
        newField.type = request.input('type')
        await newField.related('venue').associate(venue)

        return response.created({ status: 'success', data: newField })
    }

    public async show({ params, request, response }: HttpContextContract) {
        const field = await Field.query().where('id', params.id).preload('bookings', (bookingQuery) => {
            bookingQuery.select(['title', 'play_date_start', 'play_date_end'])
        }).firstOrFail()

        return response.ok({ status: 'success', data: field })
    }
}
