import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import Database from '@ioc:Adonis/Lucid/Database';
import VeneusValidator from 'App/Validators/VeneusValidator'

// models
import Venue from 'App/Models/Venue';

// import User from 'App/Models/User'

export default class VeneusesController {
    public async index({ response, request }: HttpContextContract) {
        if (request.qs().name) {
            let name = request.qs().name
            
            // cara Query Bulder
            //let venueFiltered = await Database.from('venues').select('*').where('name', name)

            //Cara Model ORM
            let venueFiltered = await Venue.findBy("name", name)
            
            return response.status(200).json({message: 'Success get venues', data: venueFiltered})
        }

        // Cara Query Buider
        // let view_venues = await Database.from('venues').select('*')

        // Cara Model ORM
        let view_venues = await Venue.all();

        return response.status(200).json({message: 'success get venues', data: view_venues})
    }
    public async store({ request, response }: HttpContextContract) {
        try {
            await request.validate(VeneusValidator);

            // Query Builder

            // let newVenuesId = await Database.table('venues').returning('id').insert({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     phone: request.input('phone')
            // })

            // Model ORM

            // const userId = auth.user?.id
            // console.log("user id: ", userId)

            let newVenues = new Venue();
            newVenues.name = request.input('name')
            newVenues.address = request.input('address')
            newVenues.phone = request.input('phone')

            await newVenues.save()

            response.created({message: 'created!'})

            // respon Query Builder 
            // response.created({ message: 'created!', newId: newVenuesId})
        } catch (error) {
            response.badRequest({errors: error.message})
        }

        // const payload = await request.validate(VeneusValidator);
    }

    public async show({ params, response }: HttpContextContract) {
        // Model ORM 
        let getvenue = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        return response.ok({ message: 'Success get Venue', data: getvenue })
    }


    public async update({ request, response, params }: HttpContextContract) {
        let id = params.id

        // Cara Query Builder
        // let affectedRows = await Database.from('venues').where('id', id).update({
        //     name: request.input('name'),
        //     address: request.input('address'),
        //     phone: request.input('phone')
        // })

        // Cara Model ORM
        let updatevenue = await Venue.findOrFail(id)
        updatevenue.name = request.input('name')
        updatevenue.address = request.input('address')
        updatevenue.phone = request.input('phone')

        updatevenue.save()

        return response.ok({ message: 'updated!'})
    }
    public async destroy({ params, response }: HttpContextContract) {
        // Cara Query Bulder
        // await Database.from('venues').where('id', params.id).delete()
        
        // Cara Model ORM
        let hapus = await Venue.findOrFail(params.id)
        await hapus.delete()
        
        return response.ok({ message: 'deleted!' })
    }
}
